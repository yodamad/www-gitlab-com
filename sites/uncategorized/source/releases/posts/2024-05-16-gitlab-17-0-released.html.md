---
release_number: "17.0" # version number - required
title: "GitLab 17.0 released with generally available CI/CD Catalog and AI Impact analytics dashboard" # short title (no longer than 62 characters) - required
author: Gabriel Engel # author name and surname - required
author_gitlab: gabrielengel_gl # author's gitlab.com username - required
image_title: '/images/17_0/17_0-cover-image.svg' # cover image - required
description: "GitLab 17.0 released with generally available CI/CD Catalog, AI Impact analytics dashboard, hosted runners on Linux Arm, deployment detail pages, and much more!" # short description - required
twitter_image: '/images/17_0/17_0-cover-image.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Today, we are excited to announce the release of GitLab 17.0 with [generally available CI/CD Catalog](#cicd-catalog-with-components-and-inputs-now-generally-available), [AI Impact analytics dashboard](#ai-impact-analytics-in-the-value-streams-dashboard), [hosted runners on Linux Arm](#introducing-hosted-runners-on-linux-arm), [deployment detail pages](#introducing-deployment-detail-pages), and much more!

These are just a few highlights from the 60+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the unbelievable 344 contributions you provided to GitLab 17.0!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

Join us to explore the new AI-powered features in GitLab 17 that will help you improve collaboration, visibility, security, and cycle times. Register for the [GitLab 17 release virtual event: The future of AI-driven software development](https://about.gitlab.com/seventeen/).

To preview what's coming in next month’s release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 17.1 release kickoff video.
