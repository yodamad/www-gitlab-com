---
layout: markdown_page
title: "Group Direction - Custom Models"
description: "Focused on grounding and enriching AI models with Customer data to produce higher quality model outputs"
canonical_path: "/direction/ai-powered/custom_models/"
---

## Overview

The Custom Models group is dedicated to empowering GitLab users with the flexibility to deploy and customize GitLab Duo Features within their local environments. We also aim to allow customers to adapt Duo Features with their own data, based on their own code, needs, requirements and styles. Our goal is to give customers a more tailored Duo experience.

We will leverage a variety of AI models to enhance GitLab Duo functionality, ensuring that responses are of the highest possible quality and tailored to the customer's source code and data. We will also work with other teams within GitLab to explore opportunities for customizing AI models across the platform.

## What Drives The Need For Custom Models?

Customers utilize GitLab in a variety of formats, including GitLab.com, Dedicated, and Self-Managed. Regardless of how customers access GitLab, they deserve the chance to leverage Duo features in a way that is tailored to their organization.

Customers have a wide variety of security and compliance requirements. These requirements often preclude them from utilizing our existing Duo features that are enabled through GitLab.com. Providing customers with a self-hosted deployment option for Duo features will allow them to reap the benefits of AI while meeting their security and compliance requirements.

Customers want to adapt their Duo experience more directly to their own organization. For example, a customer may want the ability to adapt Code Suggestions to be based on their own code base so that any suggestions match their unique coding styles, standards, and programming language of preference. In addition, customers want to be able to orient Duo Chat to their internal knowledge bases to answer questions on their own documentation.

## Strategy

The Custom Models team had dual foci of 1) enabling GitLab Duo features by leveraging customer-hosted open source (OS) models and 2) enabling customization of Duo features grounded in customer data.

### Self-Hosted Models

Custom Models is actively iterating on self-hosted support to Code Suggestions, and will be exploring self-hosted support to Duo Chat in the near term as well. Our aspirational goal for support to self-hosted (open-source based) versions of a [Duo feature](https://docs.gitlab.com/ee/user/ai_features.html)  is to release a self-hosted version of each Gitlab Duo feature in line with the GA release of as yet non-GA features. Due to the complexities of building a self-hosted version of a feature, this may not always be possible. This is predicated upon pre-GA validation of the feature, and an understanding of a feature as reaching a “mature” stage. Both validation and feature maturity creates a standard upon which to build and compare self-hosted feature variants. While self-hosted Duo features may not necessarily be as performant as features based on large, provider-hosted, 3rd party models, each feature should be validated as generally correct, useful, and reliable.  Custom Models will also factor in customer demand, feasibility, and other relevant factors when determining support.

Support for self-hosted versions of a Duo features will be predicated upon following a data-centric approach. An understanding of the domain for each feature, as well as the performance of available open source models within that domain, are essential to this approach. Prior to developing self-hostable Duo features, the Custom Models team will first identify potential OS foundation models within the required domain space. Once Custom Models has identified the optimal OS model for a feature, we will work with the AI Validation team to baseline the performance of that model or models. Custom Models will then develop prompts specific to that model, and determine the appropriate architecture. To the extent that it is possible and sensical, we will attempt to leverage existing elements of the feature that we are seeking to adapt to self-hosting (i.e. pre- and post-processing, applicable data flows, etc).

### Customization

Not all Duo features will benefit from a customized approach and we are assessing the added value of customization for several of the Duo features.

## Focus

### Identify and validate models that can be customized using the following criteria:

* open source
* minimal computing requirements
* well suited for Duo feature use cases
* high quality output

### Deeply understand customer privacy and security concerns associated with AI/ML use cases

### Develop Blueprints For Configuring Self Hosted Models

* Perform customer research to Inventory which GitLab deployments customers are using (Gitlab.com, Dedicated, Self-Managed) and how they would like to deploy self-hosted models.
* Determine architectures for enabling Enterprise users to leverage customized models.
* Assess what security and compliance requirements will we need to consider based on our customer platforms and specifications.

### Research techniques to enable customer customization of models

* How can we allow customization without sacrificing the quality of Duo features?
* Techniques under consideration include:
  * Prompt engineering
  * RAG
  * Fine-Tuning

### Validate Customer Modification

* Develop or identify a lightweight validation framework that can give customers assurances that customized product is performing at a high level of quality output and haven’t degraded performance of the model

### What We Are Not Doing

* Bring your own model - we are not currently supporting any and all model; we need to ensure the quality of the features and so will be starting with a set of pre-defined model options.
* We are not supporting personalization at an individual user level, but rather Enterprise-level customization.